package ru.nirinarkhova.tm;

import ru.nirinarkhova.tm.bootstrap.Bootstrap;
import ru.nirinarkhova.tm.exception.system.UnknownArgumentException;

public class Application{

    public static void main(String[] args) throws UnknownArgumentException {
        final Bootstrap bootstrap = new Bootstrap();
        bootstrap.run(args);
    }

}
