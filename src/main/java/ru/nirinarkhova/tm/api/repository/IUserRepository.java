package ru.nirinarkhova.tm.api.repository;

import ru.nirinarkhova.tm.model.User;

import java.util.List;

public interface IUserRepository {

    List<User> findAll();

    User add(final User user);

    User findById(final String id);

    User findByLogin(final String login);

    User findByEmail(final String login);

    User removeUser(final User user);

    User removeById(final String id);

    User removeByLogin(final String Login );

}
