package ru.nirinarkhova.tm.command.task;

import ru.nirinarkhova.tm.command.AbstractTaskCommand;
import ru.nirinarkhova.tm.enumerated.Status;
import ru.nirinarkhova.tm.exception.entity.TaskNotFoundException;
import ru.nirinarkhova.tm.model.Task;
import ru.nirinarkhova.tm.util.TerminalUtil;

import java.util.Arrays;

public class TaskChangeStatusByNameCommand extends AbstractTaskCommand {

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String name() {
        return "task-change-status-by-name";
    }

    @Override
    public String description() {
        return "change task status by task name.";
    }

    @Override
    public void execute() {
        System.out.println("[CHANGE TASK STATUS]");
        System.out.println("[ENTER TASK NAME:]");
        final String name = TerminalUtil.nextLine();
        System.out.println("[ENTER TASK STATUS:]");
        System.out.println(Arrays.toString(Status.values()));
        final String statusId = TerminalUtil.nextLine();
        final Status status = Status.valueOf(statusId);
        final Task task = serviceLocator.getTaskService().findOneByName(name);
        if (task == null) throw new TaskNotFoundException();
        final Task taskStatusUpdate = serviceLocator.getTaskService().changeTaskStatusByName(name, status);
        if (taskStatusUpdate == null) throw new TaskNotFoundException();
    }

}

