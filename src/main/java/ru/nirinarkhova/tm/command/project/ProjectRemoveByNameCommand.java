package ru.nirinarkhova.tm.command.project;

import ru.nirinarkhova.tm.command.AbstractProjectCommand;
import ru.nirinarkhova.tm.exception.entity.ProjectNotFoundException;
import ru.nirinarkhova.tm.model.Project;
import ru.nirinarkhova.tm.util.TerminalUtil;

public class ProjectRemoveByNameCommand extends AbstractProjectCommand {

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String name() {
        return "project-remove-by-name";
    }

    @Override
    public String description() {
        return "delete a project by name.";
    }

    @Override
    public void execute() {
        System.out.println("[REMOVE PROJECT]");
        System.out.println("[ENTER PROJECT NAME]");
        final String name = TerminalUtil.nextLine();
        final Project project = serviceLocator.getProjectService().removeOneByName(name);
        if (project == null) throw new ProjectNotFoundException();
    }

}

