package ru.nirinarkhova.tm.exception.system;

import ru.nirinarkhova.tm.exception.AbstractException;

public class AccessDeniedException extends AbstractException {

    public AccessDeniedException() {
        super("Error! Incorrect Email or Password...");
    }

}
